Source: python-hdmedians
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: python
Testsuite: autopkgtest-pkg-python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-numpy3,
               dh-sequence-python3,
               cython3,
               pybuild-plugin-pyproject,
               python3-all-dev,
               python3-pytest,
               python3-pytest-cov,
               python3-numpy,
               python3-setuptools
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-hdmedians
Vcs-Git: https://salsa.debian.org/python-team/packages/python-hdmedians.git
Homepage: https://github.com/daleroberts/hdmedians
Rules-Requires-Root: no

Package: python3-hdmedians
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${python3:Depends}
Description: high-dimensional medians in Python3
 Various definitions for a high-dimensional median exist and this Python
 package provides a number of fast implementations of these definitions.
 Medians are extremely useful due to their high breakdown point (up to
 50% contamination) and have a number of nice applications in machine
 learning, computer vision, and high-dimensional statistics.
 .
 This package currently has implementations of medoid and geometric
 median with support for missing data using NaN.
